<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>

# Getting started

# laravel-rdnbiotree-and-sqlite
Demo Project Laravel Linktree - B7web

## Installation

Please check the official laravel installation guide for server requirements before you start. [Official Documentation](https://laravel.com/docs/6.0/installation#installation)


Clone the repository

    git clone git@gitlab.com:rodineiti/laravel-rdnbiotree-and-sqlite.git

Switch to the repo folder

    cd laravel-rdnbiotree-and-sqlite

Install all the dependencies using composer and npm

    composer install
    npm install && npm run dev

Copy the example env file and make the required configuration changes in the .env file

    cp .env.example .env
    
Set Database SQLITE in .env

    DB_CONNECTION=sqlite
    DB_HOST=
    DB_PORT=
    DB_DATABASE=
    DB_USERNAME=
    DB_PASSWORD=

Generate a new application key

    php artisan key:generate
    
DUMP database file barbers.sql

Run the database migrations (**Set the database connection in .env before migrating**)

    php artisan migrate

Start the local development server

    php artisan serve

You can now access the server at http://localhost:8000
