<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Link extends Model
{
    use HasFactory;

    protected $fillable = ['title','href','bg_color','bg_text_color','bg_border_type','status','order'];

    public function page()
    {
    	return $this->belongsTo(Page::class);
    }
}
