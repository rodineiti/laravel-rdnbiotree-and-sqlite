<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Page;

class PageController extends Controller
{
    public function index($slug)
    {
    	$slug = explode('/', $slug);

    	if (count($slug) > 1) {
    		return back();
    	}

        $page = Page::where('slug', $slug[0])->first();

        if (!$page) {
        	return view('404');
        } else {

        	$background = '#FFFFFF';

        	if ($page->bg_type === 'image') {
                $background = "{{ asset('uploads/images/' . $page->bg_value) }}";
        	} else {
        		$colors = explode(',', $page->bg_value);
        		$bg = $colors[1] ?? $colors[0];
                $background = 'linear-gradient(90deg, '.$colors[0].', '. $bg .')';
            }

            $view = $page->views()->firstOrNew(['date_view' => date('Y-m-d')]);
            $view->total++;
            $view->save();

        	return view('page', compact('page', 'background'));
        }
    }
}
