<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $pages = auth()->user()->pages()->get();
        return view('home', compact('pages'));
    }

    public function links($page_id)
    {
        $page = auth()->user()->pages()->findOrFail($page_id);
        $links = $page->links()->orderBy('order', 'ASC')->get();
        $url = 'links';
        return view('links', compact('page','links','url'));
    }

    public function design($page_id)
    {
        $page = auth()->user()->pages()->findOrFail($page_id);
        $links = $page->links()->get();
        $url = 'design';
        return view('links', compact('page','links','url'));
    }

    public function stats($page_id)
    {
        $page = auth()->user()->pages()->findOrFail($page_id);
        $links = $page->links()->get();
        $url = 'stats';
        return view('links', compact('page','links','url'));
    }

    public function orderLinks($page_id, $link_id, $new_index)
    {
        $page = auth()->user()->pages()->findOrFail($page_id);
        $link = $page->links()->findOrFail($link_id);

        if ($link->order > $new_index) {
            foreach ($page->links()->where('order', '>=', $new_index)->get() as $value) {
                $value->order++;
                $value->save();
            }
        } else if ($link->order < $new_index) {
            foreach ($page->links()->where('order', '<=', $new_index)->get() as $value) {
                $value->order--;
                $value->save();
            }
        }
        $link->order = $new_index;
        $link->save();

        foreach ($page->links()->orderBy('order', 'ASC')->get() as $key => $value) {
            $value->order = $key;
            $value->save();
        }

        return response()->json(["error" => false, "message" => "ordernados"]);
    }

    public function newLink($page_id)
    {
        $page = auth()->user()->pages()->findOrFail($page_id);
        return view('new-link', compact('page'));
    }

    public function saveLink(Request $request, $page_id)
    {
        $request->validate([
            'title' => ['required','min:2'],
            'href' => ['required','url'],
            'bg_color' => ['required','regex:/^[#][0-9A-F]{3,6}$/i'],
            'bg_text_color' => ['required','regex:/^[#][0-9A-F]{3,6}$/i'],
            'bg_border_type' => ['required',Rule::in(['square','rounded'])],
            'status' => ['required','boolean'],
        ]);

        $page = auth()->user()->pages()->findOrFail($page_id);
        $data = $request->all();
        $data['order'] = $page->links()->count();
        $page->links()->create($data);

        session()->flash('success', 'Link criado com sucesso!');

        return redirect()->route('links', [$page->id]);
    }

    public function editLink($page_id, $link_id)
    {
        $page = auth()->user()->pages()->findOrFail($page_id);
        $link = $page->links()->findOrFail($link_id);
        return view('edit-link', compact('page','link'));
    }

    public function updateLink(Request $request, $page_id, $link_id)
    {
        $request->validate([
            'title' => ['required','min:2'],
            'href' => ['required','url'],
            'bg_color' => ['required','regex:/^[#][0-9A-F]{3,6}$/i'],
            'bg_text_color' => ['required','regex:/^[#][0-9A-F]{3,6}$/i'],
            'bg_border_type' => ['required',Rule::in(['square','rounded'])],
            'status' => ['required','boolean'],
        ]);

        $page = auth()->user()->pages()->findOrFail($page_id);
        $link = $page->links()->findOrFail($link_id);

        $data = $request->all();        
        $link->update($data);

        session()->flash('success', 'Link atualizado com sucesso!');

        return redirect()->route('links', [$page->id]);
    }

    public function destroyLink($page_id, $link_id)
    {
        $page = auth()->user()->pages()->findOrFail($page_id);
        $link = $page->links()->findOrFail($link_id);

        $link->delete();

        foreach ($page->links()->orderBy('order', 'ASC')->get() as $key => $value) {
            $value->order = $key;
            $value->save();
        }

        return redirect()->route('links', [$page->id]);
    }
}
