@extends('layouts.app')

@section('content')
<div class="col-md-10">
    <div class="panel panel-default">
        <div class="panel-body">
            <h2>Sua páginas</h2>
            <table class="table table-hover">
                <thead>
                    <th>Título</th>
                    <th>Ações</th>
                </thead>
                <tbody>
                @if(count($pages) > 0)
                    @foreach($pages as $page)
                        <tr>
                            <td>{{ $page->title }} ({{$page->slug}})</td>
                            <td>
                                <a href="{{ url('/' . $page->slug) }}" class="btn btn-xs btn-info" target="_blank">
                                    Ver
                                </a>
                                <a href="{{ route('links', $page->id) }}" class="btn btn-xs btn-warning">
                                    Links
                                </a>
                                <a href="#" class="btn btn-xs btn-success">
                                    Estatísticas
                                </a>
                                <a href="#" class="btn btn-xs btn-info">
                                    Editar
                                </a>
                                <a href="#" class="btn btn-xs btn-danger">
                                    Deletar
                                </a>
                            </td>
                        </tr>
                    @endforeach
                @else
                    <tr><td>Nenhuma página cadastrada</td></tr>
                @endif
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
