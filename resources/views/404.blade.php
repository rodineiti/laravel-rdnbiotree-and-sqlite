@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Page Not Found') }}</div>

                <div class="card-body">
                    <div class="alert alert-danger" role="alert">
                        {{ __('Page Not Found') }}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
