@extends('layouts.app')

@section('title', 'Rdnbiotree - ' . $page->title . ' - Links')

@section('content')
<div class="col-md-8">
    <div class="panel panel-default">
        <h2>Links da página {{ $page->title }}</h2>
        <ul class="nav nav-pills">
          <li class="nav-item mb-2">
            <a class="nav-link @if($url === 'links') active @endif" href="{{ route('links', $page->id) }}">Links</a>
          </li>
          <li class="nav-item">
            <a class="nav-link @if($url === 'design') active @endif" href="{{ route('design', $page->id) }}">Aparência</a>
          </li>
          <li class="nav-item">
            <a class="nav-link @if($url === 'stats') active @endif" href="{{ route('stats', $page->id) }}">Estatísticas</a>
          </li>
        </ul>
        <div class="panel-body border p-2">
            <a href="{{ route('new.link', $page->id) }}" class="btn btn-xs btn-primary mb-2">
                Adicionar link
            </a>

            <ul class="list-group" id="links">
                @if(count($links) > 0)
                    @foreach($links as $link)
                      <li class="list-group-item d-flex justify-content-between align-items-center mb-3" 
                        data-linkId="{{ $link->id }}" data-pageId="{{ $page->id }}">
                        <p style="display: flex;flex-direction: row;align-items: center;">
                            <img style="width: 25px; height: 25px; cursor: pointer;" src="{{asset('uploads/images/sort.png')}}" alt="sort">
                            <span style="margin-left: 10px;">{{ $link->title }} <br>{{ $link->href }} ordem: {{ $link->order }}</span>
                        </p>
                        <div>
                            <a href="{{ route('edit.link', [$page->id, $link->id]) }}" class="btn btn-xs btn-info">
                                Editar
                            </a>
                            <a href="{{ route('destroy.link', [$page->id, $link->id]) }}" 
                                class="btn btn-xs btn-danger" onclick="return confirm('Deseja realmente excluir este link?')">
                                Deletar
                            </a>
                        </div>
                      </li>
                    @endforeach
                @else
                    <div class="alert alert-info">Nenhum link cadastrado</div>
                @endif
            </ul>
        </div>
    </div>
</div>
<div class="col-md-2">
    <div class="card" style="width: 340px;">
        <iframe 
            src="{{ url('/' . $page->slug) }}" 
            frameborder="0"
            style="width:300px;height: 550px;border:10px solid #000;border-radius:20px;margin:20px;"
        ></iframe>
    </div>
</div>
@push('scripts')
<script src="https://cdn.jsdelivr.net/npm/sortablejs@latest/Sortable.min.js"></script>
<script>
    new Sortable(document.querySelector("#links"), {
        animation: 150,
        onEnd: async (event) => {
            const link_id = event.item.getAttribute("data-linkId");
            const page_id = event.item.getAttribute("data-pageId");
            const link = `{{ url('order/${page_id}/${link_id}/${event.newIndex}') }}`;
            await fetch(link);
            window.location.reload();
        }
    });
</script>
@endpush
@endsection
