@extends('layouts.site')

@section('title', $page->title)

@push('styles')
    <style>
        body {
            display: flex;
            flex-direction: column;
            margin: 0;
            padding: 20px;
            font-family: Helvetica, Arial;
            color: {{ $page->font_color }};
            text-align: center;
            background: {{ $background }};
        }
        .profile img {
            width: auto;
            height: 100px;
        }
        .title {
            font-size: 17px;
            font-weight: bold;
            margin-top: 10px;
        }
        .description {
            font-size: 15px;
            margin-top: 5px;
        }
        .links {
            width: 100%;
            margin: 50px 0;
        }
        .links a {
            display: block;
            padding: 20px;
            text-align: center;
            text-decoration: none;
            font-size: 18px;
            font-weight: bold;
            margin-bottom: 20px;
        }
        .links a.link-square {
            border-radius: 0px;
        }
        .links a.link-rounded {
            border-radius: 50px;
        }
        .banner a {
            color: {{ $page->font_color }};
        }
    </style>
@endpush

@section('content')
<div class="profile">
    <img src="{{ asset('uploads/images/' . $page->image_profile) }}" alt="{{ $page->title }}" />
</div>
<div class="title">
    {{$page->title}}
</div>
<div class="description">
    {{$page->description}}
</div>
<div class="links">
    @if ($page->links()->where('status', 1)->count())
        @foreach($page->links()->where('status', 1)->orderBy('order', 'ASC')->get() as $link)
            <a href="{{ $link->href }}" class="link-{{ $link->bg_border_type }}" style="background-color: {{$link->bg_color}};color: {{$link->bg_text_color}};" target="_blank">
                {{ $link->title }}
            </a>
        @endforeach
    @endif
</div>
<div class="banner">
    Desenvolvido por <a href="#">Site</a>
</div>

@if (!empty($page->pixel_facebook))
    @push('scripts')
        <!-- Facebook Pixeç Code -->
        <script>
            !function(f,b,e,v,n,t,s)
            {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
                n.callMethod.apply(n,arguments):n.queue.push(arguments)};
                if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
                n.queue=[];t=b.createElement(e);t.async=!0;
                t.src=v;s=b.getElementsByTagName(e)[0];
                s.parentNode.insertBefore(t,s)}(window, document, 'script',
                'https://connect.facebook.net/en_US/fbevents.js');
                fbq('init', '{{$page->pixel_facebook}}');
                fbq('track', 'PageView');
        </script>
        <noscript><img height="1" width="1" style="display:none" 
        src="https://www.facebook.com/tr?=id={{$page->pixel_facebook}}&ev=PageView&noscript=1"
        /></noscript>
        <!-- End Facebook Pixel Code -->
    @endpush
@endif
@endsection
