@extends('layouts.app')

@section('title', 'Rdnbiotree - ' . $page->title . ' - Novo link')

@section('content')
<div class="col-md-10">
    <div class="card card-default">
        <div class="card-body">
            <h5 class="card-title">
                Criar novo link
            </h5>

            @if($errors->any())
                <ul class="list-group">
                    @foreach($errors->all() as $error)
                        <li class="list-group-item text-danger">{{ $error }}</li>
                    @endforeach
                </ul>
            @endif

            <form action="{{ route('save.link', $page->id) }}" method="post">
                @csrf

                <div class="form-group">
                    <label for="title">Título</label>
                    <input type="text" name="title" class="form-control" required />
                </div>
                <div class="form-group">
                    <label for="href">Link</label>
                    <input type="text" name="href" class="form-control" required />
                </div>
                <div class="form-group">
                    <label for="bg_color">Cor de fundo</label>
                    <input type="color" name="bg_color" class="form-control" value="#FFFFFF" required />
                </div>
                <div class="form-group">
                    <label for="bg_text_color">Cor do texto</label>
                    <input type="color" name="bg_text_color" class="form-control" value="#000000" required />
                </div>
                <div class="form-group">
                    <label for="bg_border_type">Tipo de borda</label>
                    <select class="form-control" name="bg_border_type" required>
                        <option value="">Selecione</option>
                        <option value="square">Quadrado</option>
                        <option value="rounded">Arrendondado</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="status">Status</label>
                    <select class="form-control" name="status" required>
                        <option value="">Selecione</option>
                        <option value="1">Ativo</option>
                        <option value="0">Inativo</option>
                    </select>
                </div>
                <div class="form-group">
                    <div class="text-center">
                        <button class="btn btn-success" type="submit"> Salvar</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
