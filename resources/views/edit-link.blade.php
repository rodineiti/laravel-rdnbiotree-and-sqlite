@extends('layouts.app')

@section('title', 'Rdnbiotree - ' . $page->title . ' - Novo link')

@section('content')
<div class="col-md-10">
    <div class="card card-default">
        <div class="card-body">
            <h5 class="card-title">
                Editar link {{ $link->title }}
            </h5>

            @if($errors->any())
                <ul class="list-group">
                    @foreach($errors->all() as $error)
                        <li class="list-group-item text-danger">{{ $error }}</li>
                    @endforeach
                </ul>
            @endif

            <form action="{{ route('update.link', [$page->id, $link->id]) }}" method="post">
                @csrf
                @method('PUT')

                <div class="form-group">
                    <label for="title">Título</label>
                    <input type="text" name="title" class="form-control" value="{{ $link->title }}" required />
                </div>
                <div class="form-group">
                    <label for="href">Link</label>
                    <input type="text" name="href" class="form-control" value="{{ $link->href }}" required />
                </div>
                <div class="form-group">
                    <label for="bg_color">Cor de fundo</label>
                    <input type="color" name="bg_color" class="form-control" value="{{ $link->bg_color }}" required />
                </div>
                <div class="form-group">
                    <label for="bg_text_color">Cor do texto</label>
                    <input type="color" name="bg_text_color" class="form-control" value="{{ $link->bg_text_color }}" required />
                </div>
                <div class="form-group">
                    <label for="bg_border_type">Tipo de borda</label>
                    <select class="form-control" name="bg_border_type" required>
                        <option value="">Selecione</option>
                        <option value="square" @if($link->bg_border_type == 'square') selected @endif>Quadrado</option>
                        <option value="rounded" @if($link->bg_border_type == 'rounded') selected @endif>Arrendondado</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="status">Status</label>
                    <select class="form-control" name="status" required>
                        <option value="">Selecione</option>
                        <option value="1" @if($link->status == '1') selected @endif>Ativo</option>
                        <option value="0" @if($link->status == '0') selected @endif>Inativo</option>
                    </select>
                </div>
                <div class="form-group">
                    <div class="text-center">
                        <button class="btn btn-success" type="submit"> Atualizar</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
