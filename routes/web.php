<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])
	->name('home');
Route::get('/links/{page_id}', [App\Http\Controllers\HomeController::class, 'links'])
	->name('links');

Route::get('/order/{page_id}/{link_id}/{new_index}', [App\Http\Controllers\HomeController::class, 'orderLinks'])
	->name('order.links');

Route::get('/newLink/{page_id}', [App\Http\Controllers\HomeController::class, 'newLink'])
	->name('new.link');
Route::post('/saveLink/{page_id}', [App\Http\Controllers\HomeController::class, 'saveLink'])
	->name('save.link');

Route::get('/editLink/{page_id}/{link_id}', [App\Http\Controllers\HomeController::class, 'editLink'])
	->name('edit.link');
Route::put('/updateLink/{page_id}/{link_id}', [App\Http\Controllers\HomeController::class, 'updateLink'])
	->name('update.link');
Route::get('/destroyLink/{page_id}/{link_id}', [App\Http\Controllers\HomeController::class, 'destroyLink'])
	->name('destroy.link');

Route::get('/design/{page_id}', [App\Http\Controllers\HomeController::class, 'design'])
	->name('design');
Route::get('/stats/{page_id}', [App\Http\Controllers\HomeController::class, 'stats'])
	->name('stats');

Route::fallback([App\Http\Controllers\PageController::class, 'index']);

